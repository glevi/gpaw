.. _Niflheim:

========
Niflheim
========

Information about the Niflheim cluster can be found at
`<https://niflheim-users.readthedocs.io/>`_ and
`<https://niflheim-system.readthedocs.io/>`_.

There are two ways to use GPAW on Niflheim.  You can use a preinstalled GPAW,
or you can compile it yourself (the latter is mainly for GPAW developpers).

* If you compile it yourself, please refer to the guide
  :ref:`Compiling GPAW on Niflheim <build on niflheim>`.

* If you want to run a precompiled module, please read
  :ref:`Loading GPAW modules on Niflheim <load on niflheim>`.


If you want to learn how to run GPU version of GPAW, plese read
:ref:`Running with A100 nodes on Niflheim <gpu on niflheim>`.
Ffor now, there are now precompiled modules, so you need to build GPAW yourself.
